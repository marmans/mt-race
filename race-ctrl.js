﻿"use strict";

angular.module("smartplayer").controller("RaceCtrl", [
	"$scope",
	"$window",
	"$timeout",
	"LangServ",
	"cuteTimerServ",
	"cuteToolbarServ",
	function RaceCtrl($scope, $window, $timeout, LangServ, cuteTimerServ, cuteToolbarServ) {
		var playerTimeout,
			ledTimeout,
			active = false,
			config = cute.playerContent.config;

		const lanesAmount = 5;
		const myItemInitialLane = 3;
		const itemWidth = 16;
		const itemMargin = 3.2;
		const itemHeight = 50;
		const initialSpeedCoefficient = 0.01;
		const fps = 150;
		const amountOfNpcItems = 4;
		const startPositionFrom = -40;
		const startPositionTo = -25;
		const myItemHolderPart = 0.4;

		let fpsInterval, startTime, now, then, elapsed;
		let myRq;
		let emptyLane = myItemInitialLane;
		let isCrashed = false;
		let toSpeedUp = false;
		let roundNumber = 0;
		let amountPixelsInPercent;
		let speedCoefficient;
		let roundStartTime;
		let dragActive;
		let currentX;
		let initialX;
		let xOffset = 0;
		let shiftX = 0;

		var allNpcItems = [];

		class BaseClass {
			constructor(id) {
				this.id = id;
			}
			get htmlElement() {
				if (!this._htmlElement) {
					this._htmlElement = document.getElementById(this.id);
				}
				return this._htmlElement;
			}
		}

		class Track extends BaseClass {
			constructor(id, type) {
				super(id);
				this.type = type;
			}

			get trackOffsetWidth() {
				return this.htmlElement.offsetWidth;
			}
			get trackOffsetLeft() {
				return this.htmlElement.getBoundingClientRect().left;
			}
		}

		class Item extends BaseClass {
			constructor(id, img) {
				super(id);
				this.img = img;
				this._margin = itemMargin;
				this.height = itemHeight;
				this.width = itemWidth;
				this.bottomPx = 0;
				this.topPx = 0;
				this.leftPx = 0;
				this.rightPx = 0;
				this.heightPx = 0;
			}
			get lane() {
				if (!this._lane) {
					throw "set lane";
				}
				return this._lane;
			}
			set lane(x) {
				if (x < 1 || lanesAmount < x) {
					throw `lane must be from 1 to ${lanesAmount}`;
				}
				this._lane = x;
			}
			get leftPosition() {
				if (this._lane) {
					return this._margin * this._lane + this.width * (this._lane - 1);
				}
			}
		}

		class NpcItem extends Item {
			constructor(id, img) {
				super(id, img);
				this.top = 0;
			}
			get startPosition() {
				if (!this._lane) {
					throw "set startPosition";
				}
				return this._startPosition;
			}
			set startPosition(x) {
				if (x >= 0) {
					throw "startPosition must be negative";
				}
				this._startPosition = x;
			}
		}

		function initRace() {
			$scope.track = new Track("track");
			angular.element($window).bind("resize", function() {
				onResize();
				// manuall $digest is required as resize event outside of angular
				$scope.$digest();
			});
			initNpcItems();
			initMyItem();
		}

		function onResize() {
			amountPixelsInPercent = $scope.track.trackOffsetWidth / 100;
		}

		function initNpcItems() {
			if ($scope.type === 1) {
				allNpcItems = [
					new NpcItem("Item-1", "carObstacle1"),
					new NpcItem("Item-2", "carObstacle2"),
					new NpcItem("Item-3", "carObstacle3"),
					new NpcItem("Item-4", "carObstacle4"),
					new NpcItem("Item-5", "carObstacle5"),
					new NpcItem("Item-6", "carObstacle6"),
					new NpcItem("Item-7", "carObstacle7")
				];
			} else if ($scope.type === 2) {
				allNpcItems = [
					new NpcItem("Item-1", "airObstacle1"),
					new NpcItem("Item-2", "airObstacle2"),
					new NpcItem("Item-3", "airObstacle3"),
					new NpcItem("Item-4", "airObstacle4"),
					new NpcItem("Item-5", "airObstacle5"),
					new NpcItem("Item-6", "airObstacle6"),
					new NpcItem("Item-7", "airObstacle7")
				];
			} else {
				allNpcItems = [
					new NpcItem("Item-1", "ccObstacle1"),
					new NpcItem("Item-2", "ccObstacle2"),
					new NpcItem("Item-3", "ccObstacle3"),
					new NpcItem("Item-4", "ccObstacle4"),
					new NpcItem("Item-5", "ccObstacle5"),
					new NpcItem("Item-6", "ccObstacle6"),
					new NpcItem("Item-7", "ccObstacle7")
				];
			}
		}

		function initMyItem() {
			var img;
			if ($scope.type === 1) {
				img = "carHandle";
			} else if ($scope.type === 2) {
				img = "airHandle";
			} else {
				img = "ccHandle";
			}
			$scope.myItem = new Item("myItem", img);
			$scope.myItem.lane = myItemInitialLane;
			$scope.myItemStyle = {
				left: $scope.myItem.leftPosition + "%",
				width: $scope.myItem.width + "%"
			};
			if (detectSmartDevice()) {
				angular.element($scope.myItem.htmlElement).bind("touchstart", $scope.dragStart);
				angular.element($scope.myItem.htmlElement).bind("touchmove", $scope.drag);
				angular.element($scope.myItem.htmlElement).bind("touchend", $scope.dragEnd);
			}
		}

		function startNewRound() {
			$scope.npcItems = null;
			roundStartTime = new Date().getTime();
			isCrashed = false;
			toSpeedUp = false;
			speedCoefficient = initialSpeedCoefficient;
			if (myRq) {
				cancelAnimationFrame(myRq);
			}
			shuffleNpcItems();
			startMovement(fps);
		}

		function shuffleNpcItems() {
			const newLanesArray = Array.from({ length: lanesAmount }, (v, k) => k + 1);
			allNpcItems = _.shuffle(allNpcItems);

			$scope.npcItems = _.cloneDeep(allNpcItems).splice(0, 4);
			$scope.npcItems.forEach(item => {
				const index = Math.floor(Math.random() * newLanesArray.length);
				item.lane = newLanesArray[index];
				newLanesArray.splice(index, 1);
				item.startPosition = getRandomInt(startPositionFrom, startPositionTo);
			});

			// newLanesArray[0] - remaining element after .splice, equal to new empty lane
			const newEmptyLane = newLanesArray[0];
			emptyLaneShouldChangeEveryRound(newEmptyLane);
		}

		function emptyLaneShouldChangeEveryRound(newEmptyLane) {
			// 1. get random npcItem and set its lane = emptyLane, then update emptyLane
			// 2. initial value for emptyLane = myItemInitialLane,
			//    because user must change position of his item every round,
			//    even in the first round
			if (emptyLane === newEmptyLane) {
				const randomNpcIndex = Math.floor(Math.random() * (lanesAmount - 1));
				const temp = $scope.npcItems[randomNpcIndex].lane;
				$scope.npcItems[randomNpcIndex].lane = emptyLane;
				emptyLane = temp;
			} else {
				emptyLane = newEmptyLane;
			}
		}

		function startMovement(fps) {
			fpsInterval = 1000 / fps;
			then = Date.now();
			startTime = Date.now();
			myRq = requestAnimationFrame(animateMovement);
		}

		function animateMovement() {
			now = Date.now();
			elapsed = now - then;
			if (elapsed > fpsInterval) {
				then = now - (elapsed % fpsInterval);
				var progress = now - startTime;
				if (toSpeedUp) {
					speedCoefficient += initialSpeedCoefficient / progress;
				}
				var topPosition = progress * speedCoefficient;
				getBorderBox($scope.myItem);
				$scope.npcItems.forEach(item => {
					item.htmlElement.style.top = topPosition + item.startPosition + "%";
					item.top = topPosition + item.startPosition;
					getBorderBox(item);

					if (item.bottomPx >= $scope.myItem.topPx && item.topPx <= $scope.myItem.bottomPx - $scope.myItem.heightPx * myItemHolderPart) {
						if (item.rightPx >= $scope.myItem.leftPx && item.leftPx <= $scope.myItem.rightPx) {
							pushResults(0);
							crashed();
						}
					}
				});

				if ($scope.npcItems.filter(item => item.top > 100).length === amountOfNpcItems) {
					$scope.$apply(function() {
						pushResults(1);
						startNewRound();
					});
				}
			}
			if (!isCrashed) {
				myRq = requestAnimationFrame(() => {
					animateMovement();
				});
			}
		}

		function crashed() {
			isCrashed = true;
			cancelAnimationFrame(myRq);
			$scope.dragEnd();
			$scope.$apply(function() {
				$scope.counter = 1;
				$scope.classForMyItem = "crash";
			});
			var id = setInterval(() => {
				if ($scope.counter === 0) {
					$scope.$apply(function() {
						$scope.classForMyItem = "";
						startNewRound();
					});
					clearInterval(id);
				}
				$scope.$apply(function() {
					$scope.counter--;
				});
			}, 1000);
		}

		$scope.dragStart = function(e) {
			if (!amountPixelsInPercent) {
				amountPixelsInPercent = $scope.track.htmlElement.offsetWidth / 100;
			}

			if (e.type === "touchstart") {
				initialX = e.originalEvent.changedTouches[0].clientX - xOffset;
				shiftX = e.originalEvent.changedTouches[0].clientX - $scope.myItem.htmlElement.getBoundingClientRect().left;
			} else {
				initialX = e.clientX - xOffset;
				shiftX = e.clientX - $scope.myItem.htmlElement.getBoundingClientRect().left;
			}

			dragActive = true;
			document.addEventListener("mousemove", $scope.drag);
			$scope.myItem.htmlElement.onmouseup = function() {
				document.removeEventListener("mousemove", $scope.drag);
			};
		};

		$scope.dragEnd = function(e) {
			initialX = currentX;
			dragActive = false;
			toSpeedUp = true;

			$scope.myItem.lane = whichLaneIsItNow($scope.myItem.htmlElement.getBoundingClientRect().left);
			$scope.myItemStyle = {
				left: $scope.myItem.leftPosition + "%",
				width: $scope.myItem.width + "%"
			};
			currentX = 0;
			initialX = 0;
			xOffset = 0;
		};

		$scope.drag = function(event) {
			if (dragActive) {
				var mousePosition;
				if (event.type === "touchmove") {
					currentX = event.originalEvent.changedTouches[0].clientX - initialX;
					mousePosition = event.originalEvent.changedTouches[0].clientX - $scope.track.trackOffsetLeft;
				} else {
					currentX = event.pageX - initialX;
					mousePosition = event.pageX - $scope.track.trackOffsetLeft;
				}
				if (0 + $scope.myItem.width < mousePosition && mousePosition < $scope.track.trackOffsetWidth - $scope.myItem.width) {
					moveAt(mousePosition);
				}
				xOffset = currentX;
				event.preventDefault();
			}
		};

		function getRandomInt(min, max) {
			min = Math.ceil(min);
			max = Math.floor(max);
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}

		function getBorderBox(item) {
			const data = item.htmlElement.getBoundingClientRect();
			item.bottomPx = data.bottom;
			item.leftPx = data.left;
			item.topPx = data.top;
			item.rightPx = data.right;
			item.heightPx = data.height;
		}

		function moveAt(xPos) {
			$scope.myItem.htmlElement.style.left = xPos - shiftX + "px";
		}

		function checkDragEnd() {
			if (dragActive) {
				$scope.dragEnd();
			}
		}

		function pushResults(issuccess) {
			$scope.playerResult.push({
				roundnumber: ++roundNumber,
				emptylane: emptyLane,
				myitemlane: issuccess ? emptyLane : $scope.myItem.lane,
				issuccess: issuccess,
				roundtime: new Date().getTime() - roundStartTime,
				sector: cuteTimerServ.timeSector
			});
		}

		var unbind = $scope.$on("init-player-mt-race", function() {
			init();
			active = true;
			unbind();
		});

		function init() {
			cuteToolbarServ.showTimeProgress(true, true);
			cuteToolbarServ.setToolVisible("help", false);
			$scope.pageTitle = LangServ.getTextID("m_txt_player_header");
			$scope.showPlayer = true;
			window.addEventListener("mouseup", checkDragEnd);
			window.addEventListener("touchend", checkDragEnd);
			const uitype = config ? config[0].uitype : null;
			switch (uitype) {
				case "cars":
					$scope.type = 1;
					break;
				case "planes":
					$scope.type = 2;
					break;
				case "callcenter":
					$scope.type = 3;
					break;
			}
			$scope.trackType = uitype;
			initRace();
		}

		var unregister = $scope.$on("timer-runs", function() {
			if (active) {
				startNewRound();
				unregister();
			}
		});

		$scope.$on("stop-player-mt-race", function() {
			window.removeEventListener("mouseup", checkDragEnd);
			window.removeEventListener("touchend", checkDragEnd);
			$timeout.cancel(playerTimeout);
			$timeout.cancel(ledTimeout);
		});
	}
]);
